import _ from 'lodash'
import {
  FETCH_INCIDENTS,
  CREATE_INCIDENT,
  FETCH_INCIDENT,
  UPDATE_INCIDENT,
  DELETE_INCIDENT,
} from '../actions/types'

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_INCIDENTS:
      return _.mapKeys(action.payload, '_id')
    case CREATE_INCIDENT:
      return { ...state, [action.payload._id]: action.payload }
    case FETCH_INCIDENT:
      return { ...state, [action.payload._id]: action.payload }
    case UPDATE_INCIDENT:
      return { ...state, [action.payload._id]: action.payload }
    case DELETE_INCIDENT:
      return _.omit(state, action.payload)
    default:
      return state
  }
}
