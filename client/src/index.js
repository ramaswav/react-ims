import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import reduxThunk from 'redux-thunk';
import Header from './components/header';
import Footer from './components/footer';
import NoMatch from './components/nomatch';
import Welcome from './components/welcome';
import Signin from './components/auth/signin';
import Signup from './components/auth/signup';
import RequireAuth from './components/auth/require_auth';
import Profile from './components/userinfo/profile';
import Settings from './components/userinfo/settings';
import IncidentList from './components/incident/incident_list';
import IncidentNew from './components/incident/incident_new';
import IncidentDetail from './components/incident/incident_detail/index';
import IncidentMine from './components/incident/incident_mine';

import reducers from './reducers';
import { AUTH_USER } from './actions/types';
const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

const token = localStorage.getItem('token');
// If we have a token, consider the user to be signed in
if (token) {
  // We need to update application state
  store.dispatch({ type: AUTH_USER });
}

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Header />
        <div className="container" id="content">
          <Switch>
            <Route exact path='/' component={Welcome} />
            <Route path='/signin' component={Signin} />
            <Route path='/signup' component={Signup} />
            <Route path="/profile" component={RequireAuth(Profile)} />
            <Route path="/settings" component={RequireAuth(Settings)} />
            <Route exact path='/incidents' component={IncidentList} />
            <Route path='/incident/new' component={RequireAuth(IncidentNew)} />
            <Route path='/incident/:id' component={IncidentDetail} />
            <Route path='/my_incidents' component={RequireAuth(IncidentMine)} />
            <Route component={NoMatch} />
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  </Provider>
  , document.getElementById('root') || document.createElement('div')
);
