export const AUTH_USER = 'auth_user'
export const UNAUTH_USER = 'unauth_user'

export const FETCH_PROFILE = 'fetch_profile'
export const CLEAR_PROFILE = 'clear_profile'
export const UPDATE_PROFILE = 'update_profile'

export const FETCH_INCIDENTS = 'fetch_incidents'
export const CREATE_INCIDENT = 'create_incident'
export const FETCH_INCIDENT = 'fetch_incident'
export const UPDATE_INCIDENT = 'update_incident'
export const DELETE_INCIDENT = 'delete_incident'

export const CHECK_AUTHORITY = 'check_authority' // check if the user has the authority to make change to a specific post
