import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { createIncident } from '../../actions';
import axios from 'axios';
const ROOT_URL = '/api';
const STATUS_LIST=["assigned","acknowleged","processed"];

class IncidentNew extends Component {

  constructor(props) {
      super(props);
      this.state={
        "users":[],
        "show":false
      }
  }

  componentDidMount() {
    axios.get(`${ROOT_URL}/getUsers`, {
      headers: {authorization: localStorage.getItem('token')},  // require auth
    }).then(response => {
      this.setState(...this.state,{"users": response.data, "show":true})
    })
  }

  handleFormSubmit({ title, assigned_user,status, content }) {

    this.props.createIncident({ title, assigned_user, status, content }, (path) => {  // callback 1: history push
      this.props.history.push(path);
    }, (path, state) => {  // callback 2: history replace
      this.props.history.replace(path, state);
    });
  }

  renderInput = (field) => (
    <fieldset className="form-group">
      <label>{field.label}</label>
      <input
        className="form-control"
        {...field.input}
        type={field.type}
        placeholder={field.placeholder}
        required={field.required? 'required' : ''}
        disabled={field.disabled? 'disabled' : ''}
      />
    </fieldset>
  );

  renderTextEditor = (field) => (
    <fieldset className="form-group">
      <label>{field.label}</label>
      <input className="form-control" id="x" type="hidden" name="content" />
      <trix-editor input="x" {...field.input} />
    </fieldset>
  );

  renderAlert() {

    const { state } = this.props.history.location;
    const { action } = this.props.history;

    if (state && action === 'REPLACE') {
      return (
        <div className="alert alert-danger" role="alert">
          {`[${state.time}] --- `} <strong>Oops!</strong> {state.message}
        </div>
      );
    }
  }

  render() {

    const { handleSubmit } = this.props;

    return (
      <div className="post">
        {this.renderAlert()}
        <h2 className="mb-5">New Incident</h2>
        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
          <Field name="title" component={this.renderInput} type="text" label="Title:" placeholder="Enter your title" required={true} />
          
          <label>Assigned User &nbsp;</label>  
          { this.state && this.state.show &&
                                                <Field name="assigned_user" label="State" component="select">
                                                {this.state.users.map( (option) => { 
                                                console.log(option)
                                                
                                                return <option value={option.email}>{option.email}</option>
                                              

                                                })
                                                }
                                                </Field>
            }
          
          <label>&nbsp; Incident Status &nbsp;</label>  
          <Field name="status" label="State" component="select">
            {STATUS_LIST.map( (option) => { 
              
                return <option value={option}>{option}</option>
            })
          }
      
          </Field>
        
          <Field name="content" component={this.renderTextEditor} label="Content:" />
          <button action="submit" className="btn btn-primary">Create</button>
        </form>
      </div>
    );
  }
}

IncidentNew = reduxForm({
  form: 'incident_new',  // name of the form
})(IncidentNew);

export default connect(null, { createIncident })(IncidentNew);