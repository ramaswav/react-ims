import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchIncidents } from '../../actions/index';
var ReactBsTable  = require('react-bootstrap-table');
var BootstrapTable = ReactBsTable.BootstrapTable;
var TableHeaderColumn = ReactBsTable.TableHeaderColumn;

class IncidentList extends Component {

  constructor(props) {
    super(props);
    this.state = {  // component state: being read or being edited
      incidents: [],
      show:false
    };
  }
  componentDidMount() {
    this.props.fetchIncidents();
    // let incidents=[];
    // _.map(this.props.incidents, incident => {
    //   console.log(incident)
    //   let incidents_obj={
    //     id: incident._id,
    //     title:incident.title,
    //     assigned_user:incident.assigned_user,
    //     status:incident.status,
    //     date:incident.time
    //   }
    //   incidents.push(incidents_obj)
    // })
    // console.log("incidents",incidents)
    // this.setState(...this.state,{incidents: incidents, show: true})
  }

  renderTags(assigned_user) {
      return <span className="badge badge-info span-with-margin" key={assigned_user}>{assigned_user}</span>;
  }

  renderStatus(status) {
      if ( status == 'assigned') {
        return <span className="badge badge-danger span-with-margin" key={status}>{status}</span>;
      }
      else if ( status == 'acknowleged' ) {
        return <span className="badge badge-warning span-with-margin" key={status}>{status}</span>;
      }
      else if ( status == 'processed' ) {
        return <span className="badge badge-success span-with-margin" key={status}>{status}</span>;
      }
  }

  renderIncidentSummary(incident) {
    return (
      <div key={incident._id}>
        <h3>
          <Link className="link-without-underline" to={`/incident/${incident._id}`}>
            {incident.title}
          </Link>
        </h3>
        {this.renderTags(incident.assigned_user)}
        {this.renderStatus(incident.status)}
        <span className="span-with-margin text-grey"> • </span>
        <span className="span-with-margin text-grey">{incident.authorName}</span>
        <span className="span-with-margin text-grey"> • </span>
        <span className="span-with-margin text-grey">{new Date(incident.time).toLocaleString()}</span>
        <hr />
      </div>
    );
  }

  CellFormatter(cell, row) {
    return (<div><a href={"incident/"+row.id}>{cell}</a></div>);
  }

  CellFormatter2(cell, row) {
    return (<span className="badge badge-info span-with-margin" key={cell}>{cell}</span>);
  }

  CellFormatter3(cell, row) {
    if ( cell == 'assigned') {
        return <span className="badge badge-danger span-with-margin" key={cell}>{cell}</span>;
      }
      else if ( cell == 'acknowleged' ) {
        return <span className="badge badge-warning span-with-margin" key={cell}>{cell}</span>;
      }
      else if ( cell == 'processed' ) {
        return <span className="badge badge-success span-with-margin" key={cell}>{cell}</span>;
      }
  }

  render() {
    let incidents_list=[]
   _.map(this.props.incidents, incident => {
      console.log(incident)
      let incidents_obj={
        id: incident._id,
        title:incident.title,
        assigned_user:incident.assigned_user,
        status:incident.status,
        date:incident.time
      }
      incidents_list.push(incidents_obj)
    })
    console.log(incidents_list)
   return (
      <div className="post">
        <h1>Showing All Incidents</h1>
        { this.props.username =='IMS Admin' ? <Link className="btn btn-primary mb-5" to={'/incident/new'}>Create A New Incident</Link>: null }
        
         <BootstrapTable data={incidents_list} striped hover>
          <TableHeaderColumn dataFormat={this.CellFormatter} isKey dataField='id' dataSort={ true }>Id</TableHeaderColumn>
          <TableHeaderColumn dataField='title' dataSort={ true } >Title</TableHeaderColumn>
          <TableHeaderColumn dataFormat={this.CellFormatter2} dataField='assigned_user' dataSort={ true } >Assigned User</TableHeaderColumn>
          <TableHeaderColumn dataFormat={this.CellFormatter3} dataField='status' dataSort={ true }>Status</TableHeaderColumn>
           <TableHeaderColumn dataField='date' dataSort={ true }>Date</TableHeaderColumn>
        </BootstrapTable>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return { incidents: state.incidents, username: state.auth.username};
}

export default connect(mapStateToProps, { fetchIncidents })(IncidentList);