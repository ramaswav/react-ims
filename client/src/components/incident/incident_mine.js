import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchIncidentsByUserId } from '../../actions/index';

class IncidentMine extends Component {

  componentDidMount() {
    this.props.fetchIncidentsByUserId();
  }

  renderTags(tags) {
   return <span className="badge badge-info span-with-margin" key={tags}>{tags}</span>;

  }

  renderIncidentSummary(incident) {
    return (
      <div key={incident._id}>
        <h3>
          <Link className="link-without-underline" to={`/incident/${incident._id}`}>
            {incident.title}
          </Link>
        </h3>
        {this.renderTags(incident.assigned_user)}
        <span className="span-with-margin text-grey"> • </span>
        <span className="span-with-margin text-grey">{incident.authorName}</span>
        <span className="span-with-margin text-grey"> • </span>
        <span className="span-with-margin text-grey">{new Date(incident.time).toLocaleString()}</span>
        <hr />
      </div>
    );
  }

  render() {
    return (
      <div className="post">
        <h2 className="mb-5">incidents Asssigned to me</h2>
        {_.map(this.props.incidents, incident => {
          return this.renderIncidentSummary(incident);
        })}
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state)
  return { incidents: state.incidents };
}

export default connect(mapStateToProps, { fetchIncidentsByUserId })(IncidentMine);