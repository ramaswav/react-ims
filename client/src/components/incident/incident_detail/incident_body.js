import React, { Component } from 'react';

class IncidentBody extends Component {

  renderTags(tags) {
    return <span className="badge badge-info span-with-margin" key={tags}>{tags}</span>;
  }

  render() {

    const {incident} = this.props;
     return (
      <div>
        <h3>{incident.title}</h3>
        {this.renderTags(incident.assigned_user)}
        <span className="span-with-margin"> • </span>
        <span className="span-with-margin">{incident.authorName}</span>
        <span className="span-with-margin"> • </span>
        <span className="span-with-margin">{new Date(incident.time).toLocaleString()}</span>
        <hr />
        <div className="text-justify" dangerouslySetInnerHTML={{ __html: incident.content }} />
        <hr />
      </div>
    );
  }
}

export default IncidentBody;