import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { updateIncident } from '../../../actions';
import axios from 'axios';
const ROOT_URL = '/api';
const STATUS_LIST=["assigned","acknowleged","processed"];

class IncidentEdit extends Component {

  constructor(props) {
    super(props);
    this.state={
      "users":[],
      "show":false
    }
  }

  componentDidMount() {
    const { content } = this.props.incident;
    document.querySelector("trix-editor").value = content;
    // this.setState(...this.state, {users:results})
    axios.get(`${ROOT_URL}/getUsers`, {
      headers: {authorization: localStorage.getItem('token')},  // require auth
    }).then(response => {
      this.setState(...this.state,{"users": response.data, "show":true})
    })
  }

  handleFormSubmit({ title, assigned_user, status, content }) {
    const _id = this.props.incident._id;
    assigned_user = assigned_user.toString();

    this.props.updateIncident({ _id, title, assigned_user, status, content }, this.props.onEditSuccess, (path, state) => {
      this.props.history.replace(path, state);
    });
  }

  renderInput = (field) => (
    <fieldset className="form-group">
      <label>{field.label}</label>
      <input
        className="form-control"
        {...field.input}
        type={field.type}
        placeholder={field.placeholder}
        required={field.required? 'required' : ''}
        disabled={field.disabled? 'disabled' : ''}
      />
    </fieldset>
  );

  renderSelectInput = (field) => (
    <fieldset className="form-group">
      <label>{field.label}</label>
      <input
        className="form-control"
        {...field.input}
        type={field.type}
        placeholder={field.placeholder}
        required={field.required? 'required' : ''}
        disabled={field.disabled? 'disabled' : ''}
      />
    </fieldset>
  );

  renderTextEditor = (field) => (
    <fieldset className="form-group">
      <label>{field.label}</label>
      <input className="form-control" id="x" type="hidden" name="content" />
      <trix-editor input="x" {...field.input} />
    </fieldset>
  );

  renderAlert() {

    const { state } = this.props;
    const { action } = this.props;

    if (state && action === 'REPLACE') {
      return (
        <div className="alert alert-danger" role="alert">
          {`[${state.time}] --- `} <strong>Oops!</strong> {state.message}
        </div>
      );
    }
  }

  renderAssignedUser(){
  if ( this.props.username == 'IMS Admin') {
  return (<Field name="assigned_user" label="State" component="select">
                                                {this.state.users.map( (option) => { 
                                                  return <option value={option.email}>{option.email}</option>
                                                })
                                                }
                                                </Field>)
}
 else {
return (<Field name="assigned_user" label="State" component="select" disabled>
                                                {this.state.users.map( (option) => { 
                                                  return <option value={option.email}>{option.email}</option>
                                                })
                                                }
                                                </Field>)
} 
}

  
  render() {

    const { handleSubmit } = this.props;
    console.log(this.props.user)
    return (
      <div className="post">
        {this.renderAlert()}
        <h2 className="mb-5">Edit Your Incident</h2>
        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
         
          <Field name="title" component={this.renderInput} type="text" label="Title:" placeholder="Enter your title" required={true} />
          <label>Assigned User &nbsp;</label>  
          { this.state && this.state.show && this.renderAssignedUser()}
          
          <label>&nbsp; Incident Status &nbsp;</label>  
          <Field name="status" label="State" component="select">
            {STATUS_LIST.map( (option) => { 
              
                return <option value={option}>{option}</option>
            })
          }
          </Field>

          <Field name="content" component={this.renderTextEditor} label="Content:" />
          <button action="submit" className="btn btn-primary">submit</button>
        </form>
      </div>
    );
  }
}

IncidentEdit = reduxForm({
  form: 'incident_edit',  // name of the form
})(IncidentEdit);

function mapStateToProps(state, ownProps) {
  return { 
    initialValues: ownProps.incident,
    username: state.auth.username
  };
}

export default connect(mapStateToProps, { updateIncident })(IncidentEdit);