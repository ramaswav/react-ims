import React from 'react'

export default () => {
  return (
    <footer className="footer">
      <div className="container">
        <span className="text-muted">Vivek Ramaswamy| IMS &copy; 2019 </span>
      </div>
    </footer>
  )
}
