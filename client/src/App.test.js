
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import localStorage from './localStorage'
import Welcome from './components/welcome.js';
import Header from './components/header.js';
import Footer from './components/footer.js';
import Nomatch from './components/nomatch.js';

import { BrowserRouter as Router } from 'react-router-dom';
import { shallow, mount } from 'enzyme';
import configureMockStore from "redux-mock-store";

const mockStore = configureMockStore();
const store = mockStore({
	"authenticated":true
});


//Smoke Tests.
describe('Smoke Test for basic components', function () {

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Router><Welcome /></Router>,div);
});


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Router><Footer /></Router>,div);
});


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Router><Nomatch /></Router>,div);
});

})