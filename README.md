# React Redux Incident Management Sytem

This is a MERN stack based fully functioning Incident Management System, which supports features of signing in, signing up, making authenticated requests, updating profile, changing password, creating/editing/deleting incidents.
prettier has been used beautify the code.
## Tech Stack

#### Front-end

* The front-end client is built as a simple-page-application using React and Redux (for middlewares and reducers).
* React-Router is used for navigation.
* Redux-Thunk is used for processing asynchronous requests.
* Bootstrap 4 is used for page styling.

#### Back-end

* The back-end server is built with Express.js and Node.js in MVC pattern, which provides completed REST APIs for data interaction.
* Passport.js is used as an authentication middleware in the sever.
* JSON Web Token (JWT) is used for signing in user and making authenticated requests.

#### Database
* MongoDB is used as the back-end database, which include different data models/schemas (i.e., User, Incidents).
* Mongoose is used to access the MongoDB for CRUD actions (create, read, update and delete).
* Mongo DB cloud Atlas is used to save the data. Mongodb CloudAtlas ( https://cloud.mongodb.com/ )

## Usage

Running Via Docker ( Docker composer)

1. Install Lastest docker package on your system.
2. Navigate to root directory of this repository, there you will find docker-compose.yml.
3. RUN `docker-compose up --build`; (This would create 2 containers one each for server and client cosuming about 350 MB of disk space.)
4. Once the image are built and containers come up visit http://localhost:3000/

Running locally using npm. You need 2 terminals open: one for client, one for server, Below are the steps:

1. Install Node.js;
2. `git clone https://ramaswav@bitbucket.org/ramaswav/react-ims.git`;
3. Go to directory `client`, and run `npm install`;
4. Go to directory `server`, and run `npm install`;
5. In `server` directory, run `npm run dev`;
6. Edit he package.json to  and change "proxy": "http://server:3090/" to "proxy": "http://localhost:3090/"
7. In `client` directory, run `npm run start`;

Then you are all set. You can go to `http://localhost:3000/` to check the live application.

## Test Application Functionality  API's
1. Login to http://localhost:3000/signin  ( admin@ims.com / admin )
2. You can see list of incidents
3. You can add a new incident and assign it to a user ( currently i have created 2 users user1@ims.com and user2@ims)
4. Logout
5. Login back as a user ( user1@ims.com / user1)
6. You should be able to accknowlege the ticket/ view other ticket descriptions, update ticket assigned to you etc.

## Run Unit Tests 
* Note if you installed via docker then to run tests you need to install the packages in client and server directory via npm i

1. cd /server -- npm run test
2. cd /client -- npm run test



## Backend API's

|       Route         | METHOD |  Protected   |  Description                 |
|:--------------------|:-------|:-------------|:-----------------------------|
| /api/               | GET    |    FALSE     | Health check method          |
| /api/               | GET    |    TRUE      | Protected health check       |
| /api/signin         | POST   |    FALSE     | User signin                  |
| /api/signup         | POST   |    TRUE      | User signout                 |
| /api/verify_jwt     | GET    |    TRUE      | Token verification           |
| /api/profile        | GET    |    TRUE      | Show user profile details    |
| /api/profile        | PUT    |    TRUE      | Update user profile details  |
| /api/password       | PUT    |    TRUE      | Update password              |
| /api/incidents      | GET    |    FALSE     | List all the incidents       |
| /api/incident       | POST   |    TRUE      | Create a new incident        |
| /api/password       | PUT    |    TRUE      | Update password              |
| /api/incident/:id   | GET    |    TRUE      | Retreive a specific incident |
| /api/incident/:id   | PUT    |    TRUE      | Update a specific incident   |
| /api/incident/:id   | DELETE |    TRUE      | Delete a specific incident   |
| /api/my_incidents   | GET    |    TRUE      | List all my incidents        |