const Authentication = require('./controllers/authentication')
const Profile = require('./controllers/userinfo')
const Incident = require('./controllers/incident')

// service
const passport = require('passport')
const passportService = require('./services/passport')

// middleware in between Incoming Request and Route Handler
const requireAuth = passport.authenticate('jwt', { session: false })
const requireSignin = passport.authenticate('local', { session: false })

module.exports = function(app) {
  /**
   * Authentication APIs
   */

  app.get('/api/', function(req, res) {
    res.send({ message: 'i am alive' });
  });

  app.get('/api/', requireAuth, function(req, res) {
    res.send({ message: 'Super secret code is ABC123' })
  })

  app.post('/api/signup', Authentication.signup)

  app.post('/api/signin', requireSignin, Authentication.signin)
  // app.post('/api/signin', Authentication.signin);

  app.get('/api/verify_jwt', requireAuth, Authentication.verifyJwt)

  /**
   * Profile APIs
   */

  app.get('/api/profile', requireAuth, Profile.fetchProfile)

  app.put('/api/profile', requireAuth, Profile.updateProfile)

  app.put('/api/password', requireAuth, Profile.resetPassword)

  app.get('/api/getUsers', requireAuth, Profile.getUsers)

  /**
   * Incident Post APIs
   */

  app.get('/api/incidents', Incident.fetchIncidents)

  app.post('/api/incident', requireAuth, Incident.createIncident)

  app.get('/api/incident/:id', Incident.fetchIncident)

  app.get(
    '/api/allow_edit_or_delete/:id',
    requireAuth,
    Incident.allowUpdateOrDelete
  )

  app.put('/api/incident/:id', requireAuth, Incident.updateIncident)

  app.delete('/api/incident/:id', requireAuth, Incident.deleteIncident)

  app.get('/api/my_incidents', requireAuth, Incident.fetchIncidentsByAuthorId)
}
