const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Define our model
const incidentSchema = new Schema({
  title: String,
  assigned_user: String,
  content: String,
  status: String,
  authorId: String,
  authorName: String,
  time: Date,
})

// Create the model class.
const ModelClass = mongoose.model('incident', incidentSchema)

// Export the model.
module.exports = ModelClass
