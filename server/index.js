// Main starting point of the application
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const app = express();
const router = require('./router');
const mongoose = require('mongoose');
// const cors = require('cors');  // we don't need it anymore, because we use proxy server instead
//Cloud Atlas cluster test database
const mongoconnection_url='mongodb://mongouser:mongopassword@cluster0-shard-00-00-vxpeb.mongodb.net:27017,cluster0-shard-00-01-vxpeb.mongodb.net:27017,cluster0-shard-00-02-vxpeb.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority';
// DB Setup (connect mongoose and instance of mongodb)
mongoose.connect(mongoconnection_url);

// App Setup (morgan and body-parser are middleware in Express)
app.use(morgan('combined'));  // middleware for logging
app.use(bodyParser.json({ type: '*/*' }));  // middleware for helping parse incoming HTTP requests
// app.use(cors());  // middleware for circumventing (规避) cors error

// Router Setup
router(app);

// Server Setup
const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port);
console.log('Server listening on: ', port);
module.exports=server;