/**************************************
List of assert methods - See  http://chaijs.com/api/assert/#method_assert
                        for the full list
fail(actual, expected, msg)
equal(a, b)
notEqual
isAbove     isAtLeast
isBelow     isAtMost
typeOf
isOk        strictEqual         deepEqual
isNotOk     strictNotEqual      deepNotEqual
isTrue      isNotTrue
isFalse     isNotFalse
isNull      isNaN           isNotNull       isNotNaN
exists      notExists
isUndefined isDefined
isFunction  isObject    isArray isString    isNumber
isBoolean
instanceOf
match       notMatch        <-- regular expressions
property    notProperty
propertyVal notPropertyVal
lengthOf
hasAnyKeys  hasAllKeys      containsAllKeys <!--- in objects
throws      doesNotThrow
closeTo     approximately
oneOf       <--- look in an array
changes     doesNotChange
increases   increasesBy     increasesButNotBy
decreases   decreasesBy     decreasesButNotBy
**************************************/
// Unit Testing with Mocha and Chai
//
const assert = require('chai').assert
const app = require('../index.js')
const authentication = require('../controllers/authentication.js')
const chaiHttp = require('chai-http')
const chai = require('chai')
const expect = chai.expect
chai.use(chaiHttp)

describe('App basic tests', () => {
  it('/GET should return 200 and a message', done => {
    //send a GET request to /
    chai
      .request(app)
      .get('/api')
      .then(res => {
        //validate response has a message
        expect(res).to.have.status(200)
        expect(res.body.message).to.be.equal('i am alive')

        done()
      })
      .catch(err => {
        console.log(err)
      })
  })

  it('Returns 404 error for non defined routes', done => {
    chai
      .request(app)
      .get('/unexisting')
      .then(res => {
        expect(res).to.have.status(404)
        done()
      })
  })
})

// describe('User Signup', () => {
//   it('should return 200 and confirm user creation', (done) => {
//     //mock valid user input
//     let user_input = {
//       "email":'test@ims.com',
//       "password":'test',
//       "firstName":'test',
//       "lastName":'user'
//     }
//     //send /POST request to /register
//     chai.request(app).post('/api/signup').send(user_input).then(res => {
//       expect(res).to.have.status(200);
//       expect(res.body.message).to.be.equal('You have successfully signed up. You can sign in now.');
//       done();
//     }).catch(err => {
//       console.log(err);
//     });
//   })
// })

describe('Same User already exists', () => {
  it('should return 422 and confirm user exits', done => {
    //mock valid user input
    let user_input = {
      email: 'test@ims.com',
      password: 'test',
      firstName: 'test',
      lastName: 'user',
    }
    //send /POST request to /register
    chai
      .request(app)
      .post('/api/signup')
      .send(user_input)
      .then(res => {
        expect(res).to.have.status(422)
        expect(res.body.message).to.be.equal('This email is in use.')
        done()
      })
      .catch(err => {
        console.log(err)
      })
  })
})

describe('List incidents', () => {
  it('/GET should return 200 and a message', done => {
    //send a GET request to /
    chai
      .request(app)
      .get('/api/incidents')
      .then(res => {
        //validate response has a message
        expect(res).to.have.status(200)
        //expect(res.body.message).to.be.equal('i am alive')
        done()
      })
      .catch(err => {
        console.log(err)
      })
  })
})

describe('Create Incident', () => {
  var token = null

  before(function(done) {
    chai
      .request(app)
      .post('/api/signin')
      .send({
        email: 'testuser@ims.com',
        password: 'testuser',
      })
      .end(function(err, res) {
        token = res.body.token // Or something
        done()
      })
  })

  it('should return create a new incident', done => {
    let newIncident = {
      title: 'test title',
      assigned_user: 'testuser@ims.com',
      content: 'test',
      status: 'assigned',
    }
    chai
      .request(app)
      .post('/api/incident')
      .set('Authorization', 'JWT ' + token)
      .send(newIncident)
      .then(res => {
        //validate response has a message
        expect(res).to.have.status(500)
        done()
      })
      .catch(err => {
        console.log(err)
      })
    done()
  })
})

describe('Fetch Single Incident', () => {
  it('should fetch a single incident', done => {
    chai
      .request(app)
      .get('/api/incident/5df60c15a4ff21001cf70d29')
      .then(res => {
        //validate response has a message
        expect(res).to.have.status(200)
        done()
      })
      .catch(err => {
        console.log(err)
      })
    done()
  })
})
