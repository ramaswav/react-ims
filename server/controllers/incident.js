let _ = require('lodash')

const Incident = require('../models/incident')
/**
 * ------- incident APIs -------
 */

/**
 * Get a list of incident
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchIncidents = function(req, res, next) {
  Incident.find({})
    .select({})
    .limit(100)
    .sort({
      time: -1,
    })
    .exec(function(err, incident) {
      if (err) {
        console.log(err)
        return res.status(422).json({
          message: 'Error! Could not retrieve incident.',
        })
      }
      res.json(incident)
    })
}

/**
 * Create a new incident
 *
 * @param req
 * @param res
 * @param next
 */
exports.createIncident = function(req, res, next) {
  console.log(req.body)
  // Require auth
  const user = req.user
  const title = req.body.title
  const assigned_user = req.body.assigned_user
  const content = req.body.content
  const status = req.body.status
  const authorId = user._id
  const authorName = user.firstName + ' ' + user.lastName
  const time = Date.now()

  // Make sure title, assigned_user and content are not empty
  if (!title || !assigned_user || !content) {
    return res.status(422).json({
      message: 'Title, assigned_user and content are all required.',
    })
  }

  // Create a new incident
  const incident = new Incident({
    title: title,
    assigned_user: _.uniq(assigned_user.split(',').map(item => item.trim())), // remove leading and trailing spaces, remove duplicate assigned_user
    content: content,
    status: status,
    authorId: authorId,
    authorName: authorName,
    time: time,
  })

  // Save the incident
  incident.save(function(err, incident) {
    // callback function
    if (err) {
      return next(err)
    }
    res.json(incident) // return the created incident
  })
}

/**
 * Fetch a single incident by incident ID
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchIncident = function(req, res, next) {
  Incident.findById(
    {
      _id: req.params.id,
    },
    function(err, incident) {
      if (err) {
        console.log(err)
        return res.status(422).json({
          message:
            'Error! Could not retrieve the incident with the given incident ID.',
        })
      }
      if (!incident) {
        return res.status(404).json({
          message: 'Error! The incident with the given ID is not exist.',
        })
      }
      res.json(incident) // return the single blog incident
    }
  )
}

/**
 * Check if current incident can be updated or deleted by the authenticated user: The author can only make change to his/her own incident
 *
 * @param req
 * @param res
 * @param next
 */
exports.allowUpdateOrDelete = function(req, res, next) {
  // Require auth
  const user = req.user
  // Find the incident by incident ID
  Incident.findById(
    {
      _id: req.params.id,
    },
    function(err, incident) {
      if (err) {
        console.log(err)
        return res.status(422).json({
          message:
            'Error! Could not retrieve the incident with the given incident ID.',
        })
      }

      // Check if the incident exist
      if (!incident) {
        return res.status(404).json({
          message: 'Error! The incident with the given ID is not exist.',
        })
      }

      console.log(user._id)
      console.log(incident.authorId)

      // Check if the user ID is equal to the author ID
      if (!user._id.equals(incident.authorId)) {
        return res.send({ allowChange: false })
      }
      res.send({ allowChange: true })
    }
  )
}

/**
 * Edit/Update a incident
 *
 * @param req
 * @param res
 * @param next
 */
exports.updateIncident = function(req, res, next) {
  // Require auth
  const user = req.user
  // Find the incident by incident ID
  Incident.findById(
    {
      _id: req.params.id,
    },
    function(err, incident) {
      if (err) {
        console.log(err)
        return res.status(422).json({
          message:
            'Error! Could not retrieve the incident with the given incident ID.',
        })
      }

      // Check if the incident exist
      if (!incident) {
        return res.status(404).json({
          message: 'Error! The incident with the given ID is not exist.',
        })
      }

      // Make sure the user ID is equal to the author ID (Cause only the author can edit the incident)
      // console.log(user._id);
      // console.log(incident.authorId);
      // if (!user._id.equals(incident.authorId)) {
      //   return res.status(422).json({
      //     message: 'Error! You have no authority to modify this incident.'
      //   });
      // }

      // Make sure title, assigned_user and content are not empty
      const title = req.body.title
      const assigned_user = req.body.assigned_user
      const status = req.body.status
      const content = req.body.content
      console.log('hhf', req.body)
      if (!title || !assigned_user || !content) {
        return res.status(422).json({
          message: 'Title, assigned_user and content are all required.',
        })
      }

      // Update user
      incident.title = title
      ;(incident.assigned_user = _.uniq(
        assigned_user.split(',').map(item => item.trim())
      )), // remove leading and trailing spaces, remove duplicate assigned_user;
        (incident.status = status)
      incident.content = content

      // Save user
      incident.save(function(err, incident) {
        // callback function
        if (err) {
          return next(err)
        }
        res.json(incident) // return the updated incident
      })
    }
  )
}

/**
 * Delete a incident by incident ID
 *
 * @param req
 * @param res
 * @param next
 */
exports.deleteIncident = function(req, res, next) {
  // Require auth

  // Delete the incident
  Incident.findByIdAndRemove(req.params.id, function(err, incident) {
    if (err) {
      return next(err)
    }
    if (!incident) {
      return res.status(422).json({
        message: 'Error! The incident with the given ID is not exist.',
      })
    }

    // Return a success message
    res.json({
      message: 'The incident has been deleted successfully!',
    })
  })
}

/**
 * Fetch incident by author ID
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchIncidentsByAuthorId = function(req, res, next) {
  // Require auth
  const user = req.user

  // Fetch incident by author ID
  Incident.find({
    authorId: user._id,
  })
    .select({})
    .limit(100)
    .sort({
      time: -1,
    })
    .exec(function(err, incident) {
      if (err) {
        console.log(err)
        return res.status(422).json({
          message: 'Error! Could not retrieve incident.',
        })
      }
      res.json(incident)
    })
}
